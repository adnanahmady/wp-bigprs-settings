<?php

// define the get_archives_link callback
function filter_get_archives_link( $link_html, $url, $text, $format, $before, $after ) {
    // make filter magic happen here...
    if ('html' == $format)
        $link_html = "\t<li class='bigprs-settings-html-archive-item'>$before<a href='$url'>$text" . ( ! empty ( $after ) ? "<span class='left link-count'>$after</span>" : "" ) . "</a></li>\n";
    return $link_html;
};
// add the filter
add_filter( 'get_archives_link', 'filter_get_archives_link', 10, 6 );


add_filter('wp_list_categories', 'cat_count_span');
function cat_count_span($links) {
    $links = str_replace('</a> (', ' <span class="left link-count">(', $links);
    $links = str_replace(')', ')</span></a>', $links);
    return $links;
}


