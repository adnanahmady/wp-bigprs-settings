<?php
/**
 * Plugin Name: bigprs settings
 * Plugin URI: http://www.bigprs.com/
 * Author: عدنان احمدی
 * Author URI: http://www.bigprs.com/
 * Version: 0.1
 * Text Domain: bigprs_settings
 */

if ( ! defined ('ABSPATH') ) { exit; }

require_once plugin_dir_path ( __FILE__ ) . '/bigprs_settings_menus/bigprs_settings_vertical_menu.php' ;

require_once plugin_dir_path ( __FILE__ ) . '/bigprs_settings_manipulate_classes/bigprs_settings_recent_widget_class.php' ;

require_once plugin_dir_path ( __FILE__ ) . '/bigprs_settings_manipulate_classes/bigprs_settings-widget-recent-comments.php' ;

require_once plugin_dir_path ( __FILE__ ) . '/bigprs_settings_edit_functions/shop.php' ;

require_once plugin_dir_path ( __FILE__ ) . '/bigprs_settings_edit_functions/widgets.php' ;

require_once plugin_dir_path ( __FILE__ ) . '/bigprs_settings_widgets/search-form-widget.php' ;

