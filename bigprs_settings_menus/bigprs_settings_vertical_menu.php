<?php
/**
 *
 * bigprs vertical menu
 *
 */

register_nav_menus( array(
    'bigprs_settings_vertical_menu' => esc_html__( 'Vertical Menu bigprs settings', 'bigprs_settings' ),
) );
function bigprs_settings_vertical_menu_widget ( ) {
    wp_nav_menu( array(
        'theme_location'    => 'bigprs_settings_vertical_menu',
        'container_id'      => 'bigprs_settings_menu_container_id',
        'container_class'   => 'bigprs_settings_menu_container_class',
        'menu_id'           => 'ver_menu',
        'menu_class'        => 'bigprs-vertical-menus',
        'fallback_cb'       => true
    ) );
}
function bigprs_shop_vertical_menu_widget_init ( ) {
    // if ( ! function_exists ( 'bigprs_shop_vertical_menu' ) ) {
    //     bigprs_shop_vertical_menu () ;
    // }
    register_sidebar_widget (
        'bigprs_vertical_menu',
        'bigprs_settings_vertical_menu_widget'
    );
}
add_action ( 'widgets_init', 'bigprs_shop_vertical_menu_widget_init' ) ;
